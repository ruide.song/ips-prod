all: src doc test run_test
src:
	$(MAKE) -C src
doc:
	$(MAKE) -C doc
test:
	$(MAKE) -C ./src/test

.PHONY: clean src doc test run_test

clean:
	$(MAKE) -C src clean
	$(MAKE) -C doc clean
	cd src/test/ && $(MAKE) clean
