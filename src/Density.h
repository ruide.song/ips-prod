#ifndef DENSITY_H
#define DENSITY_H


#include "Basis.h"

using namespace std;

class Density
{
	public:
	  
	  arma::mat direct(arma::vec , arma::vec );
	  arma::mat opt_direct(arma::vec , arma::vec );
	  arma::cube df3_direct(arma::vec , arma::vec , arma::vec);
};

#endif
