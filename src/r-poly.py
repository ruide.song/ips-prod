from __future__ import division
import math  
import numpy


a = numpy.zeros(shape=(15,10),dtype = numpy.longdouble) 
for m in range(15):
    for n in range(10):
        a[m,n]= 1/math.sqrt(math.pi)*math.sqrt(math.factorial(n)/math.factorial(n+abs(m)))
     
numpy.savetxt("r.arma",a)
with open("r.arma", "r+") as f:
    old = f.read()
    f.seek(0)
    f.write("ARMA_MAT_TXT_FN008\n")
    f.write("15 10\n")
    f.write(old)

f.close()
