#ifndef POLY_H
#define POLY_H

#include<iostream>
#include<armadillo>
using namespace std;

class Poly
{
	public :
	  arma::mat her;
	  arma::cube lag;


	  arma::vec  Her_poly(int , arma::vec);
	  arma::mat  calcHermite(int, arma::vec);
	  arma::vec  hermite(int); 

	  arma::vec  Lag_poly(int ,int, arma::vec);
	  arma::cube  calcLaguerre(int, int, arma::vec);
	  arma::vec  laguerre(int,int);
	  
};

#endif	
