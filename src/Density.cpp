#include "Density.h"


/**
 * @file Density.cpp
 * @brief this file is meant to implement the calculation of the density
 * @author Ruide_SONG abdourahmane afifi
 * @date 3 January 2017
 */

using namespace std;

/**  
* This function computes directly the density 
* @param zVals a vector 
* @param rVals a vector
* @return result the density
*/


arma::mat Density::direct(arma::vec zVals, arma::vec rVals){
	arma::mat rho;
	rho.load("rho.arma", arma::arma_ascii);
	Basis basis(1.935801664793151,      2.829683956491218,     14,     1.3);
	
	int nbR = rVals.n_elem;
	int nbZ = zVals.n_elem;		

	uint i = 0;
	uint j = 0;
	arma::mat result = arma::zeros(nbR, nbZ); // number of points on r- and z- axes
	
	for (int m = 0; m < basis.mMax; m++)
	{
  		for (int n = 0; n < basis.nMax(m); n++)
  		{
    			for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
    			{
				j = 0;
      				for (int mp = 0; mp < basis.mMax; mp++)
      				{
        				for (int np = 0; np < basis.nMax(mp); np++)
        				{
          					for (int n_zp = 0; n_zp < basis.n_zMax(mp, np); n_zp++)
          					{	
            						arma::mat funcA = basis.basisFunc( m,  n,  n_z, zVals, rVals);
            						arma::mat funcB = basis.basisFunc(mp, np, n_zp, zVals, rVals);
            						result += funcA % funcB * rho(i,j); // mat += mat % mat * double
							j++;
          					}
        				}
     				 }
				 i++;
				 
    			}
  		}
	}
	return result;
}


/**  
* This function computes directly and we use an optimisation.
* @param zVals a vector 
* @param rVals a vector
* @return result the density
*/
arma::mat Density::opt_direct(arma::vec zVals, arma::vec rVals){
	arma::mat rho;
	rho.load("rho.arma", arma::arma_ascii);
	Basis basis(1.935801664793151,      2.829683956491218,     14,     1.3);
	
	int nbR = rVals.n_elem;
	int nbZ = zVals.n_elem;		

	uint i = 0;
	uint j = 0;
	uint h = 0;
	arma::cube func;
	arma::mat result = arma::zeros(nbR, nbZ); // number of points on r- and z- axes
	
	//Compute the number of value of 'func' function
	for (int m = 0; m < basis.mMax; m++) 
	{
  		for (int n = 0; n < basis.nMax(m); n++)
  		{
    			for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
    			{
				h++;
			}
		}
	}

	func.zeros(nbR,nbZ,h);
	i=0;
	
	//Store every value of 'func' function
	for (int m = 0; m < basis.mMax; m++)
	{
  		for (int n = 0; n < basis.nMax(m); n++)
  		{
    			for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
    			{
				func.slice(i)=basis.basisFunc(m, n, n_z, zVals, rVals);
				i++;
			}
		}
	}	
	

	//optimisation, Sum in two places
	for (i = 0; i < h; i++)
	{
		arma::mat result2 = arma::zeros(nbR, nbZ);
    		for (j = 0; j < h; j++)						
            		result2 += func.slice(j) * rho(i,j); // mat +=  mat * double

		result += func.slice(i) % result2; 
	}
	// r = x pour y =0

	return result;
}

arma::cube Density::df3_direct(arma::vec zVals, arma::vec xVals, arma::vec yVals){
	int nbX = xVals.n_elem;
	int nbY = yVals.n_elem;
	int nbZ = zVals.n_elem;
	arma::cube result = arma::zeros(nbX, nbZ, nbY); // number of points on r- and z- axes
	int i;
	for (i=0; i<nbY; i++){
		arma::vec rVals= sqrt(square(xVals)+yVals(i)*yVals(i));
		result.slice(i) = opt_direct(zVals,rVals);
	}
	return result;
}




