#include "Density.h"

using namespace std;
std::string cubeToDf3(const arma::cube &m)
{
  std::stringstream ss(std::stringstream::out | std::stringstream::binary);
  int nx = m.n_rows;
  int ny = m.n_cols;
  int nz = m.n_slices;
  ss.put(nx >> 8);
  ss.put(nx & 0xff);
  ss.put(ny >> 8);
  ss.put(ny & 0xff);
  ss.put(nz >> 8);
  ss.put(nz & 0xff);
  double theMin = 0.0;
  double theMax = m.max();
  for (uint k = 0; k < m.n_slices; k++)
  {
    for (uint j = 0; j < m.n_cols; j++)
    {
      for (uint i = 0; i < m.n_rows; i++)
      {
        uint v = 255 * (fabs(m(i, j, k)) - theMin) / (theMax - theMin);
        ss.put(v);
      }
    }
  }
  return ss.str();
}

int main(){
	Density density;
	arma::mat result;
	arma::cube result1;
	// we can change the z and r
	/*	arma::vec z = {-10.1, -8.4, -1.0, 0.0, 0.1, 4.3, 9.2, 13.7};
	arma::vec r = {3.1, 2.3, 1.0, 0.0, 0.1, 4.3, 9.2, 13.7};
	arma::vec y = {0,1}; 
	double fm = pow(10,-15);
	arma::vec x_axis = arma::linspace( -10, 10,100 ); */
	arma::vec y = {0,1};
	arma::vec r = arma::linspace( -10, 10,32 ) ;
	arma::vec z = arma::linspace( -10, 10,32 );
	// changer vecteurs !!

	
	result = density.opt_direct(z,r); //result of density, we can use it to plot
	result.print("result:");
	result1 = density.df3_direct(z,r,y);//zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
	result1.print("result1:");
	result.save("plotFile2D.txt",arma::raw_ascii);
	//raw ascii matrix
	
	/* if file has a header, we must remove it with the sed call
	system("sed -i '1d' plotFile2D.txt");
	system("sed -i '1d' plotFile2D.txt");
	*/


	
	system("gnuplot command_file");
	system("feh plotFile2D.png"); // affiche le résultat en 2d 

	
	return 0;
}
