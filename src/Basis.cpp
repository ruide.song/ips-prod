#include "Basis.h"
using namespace std;	
Basis::Basis(double br, double bz, int N, double Q)
{
  r_poly.load("r.arma", arma::arma_ascii);
  z_poly.load("z.arma", arma::arma_ascii);
  
  this->br=br;	
  this->bz=bz;
  this->N=N;
  this->Q=Q;
  int i=0,m,n,n_z; 
  
  while(((N+2)*pow(Q,2.0/3.0)+0.5-i*Q)>=1)
    {
      i++;
    }		
  mMax=i-1;
  // fin du calcul 

  /*On crée le vecteur m comme étant la liste des entiers de 0 à mMax
   Ensuite pour chaque élément de ce vecteur, (m_i) : on calcule une borne max n_max
   Et on cree le vecteur n comme étant la liste des maximums (bornes sup) pour chaque coordonnée 
         +-------------------------------------+  
     M : |0 |1 |2 |   |  |   |     |   |   |14 |  
         +-------------------------------------+  
         +  +  +  +                                
         |0 |0 |0 |                               
         |1 |1 |1 |                               
         |2 |2 |2 |
	 |..|..|..|
	 |4 |4 |4 |
	 |5 |5 |0 |
                     
	 +-------------------------------------+   
     n : |5 |5 |4 |4  |  |   |     |   |   |0  |   
         +-------------------------------------+   
	 Ce n'est pas un probleme d'avoir une liste des maxuimums puisqu'en 
	 fin de compte, la liste des maximums couvre l'ensemble des vraies valeurs possibles pour n
	 On construit alors une matrice pour avoir les bornes nzmax,
	 ces bornes dépendent à la fois 
	 de m, et de n donc il faut une matrice pour les stocker 

         +-------------------------------------+  
     M : |0 |1 |2 |   |  |   |     |   |   |14 |  
         +-------------------------------------+  

N:                         
|0 |     |..|..|..|.. |..|...|.....|...|...|...|  bornes pour n = 0                   
----     +-------------------------------------+   
|1 |     |..|..|n |.. |..|...|.....|...|...|...| 
----     +-------------------------------------+  
|2 |     |..|..|..|o..|..|...|.....|...|...|...|
----     +-------------------------------------+   
|3 |     |..|..|..|.. |m.|...|.....|...|...|...|
----     +-------------------------------------+  
|4 |     |..|..|..|.. |..|.r.|.....|...|...|...|
----     +-------------------------------------+  
|5 |     |..|..|..|.. |..|...|.E...|...|...|...| bornes pour n = 5 (plus généralement nmax(0))
----     +-------------------------------------+  

Si Mat(n0,m0) = x alors 
         +-----------------+ 
   n_z:  |0 |1 |..|..|..|x |  
         +-----------------+ 
c'est la même chose, on a un vecteur de maximum mais ce n'est pas un problème.
Pour un x,y donné n_z va de 0 à x
x = nzmax(m - 2n + 1 ) -1 */





  
  nMax.zeros(mMax);
  for(m=0; m < mMax; m++)
    {
      nMax(m)=(mMax-m-1)/2;
    }
  nMax++;
  
  n_zMax.zeros(mMax,nMax(0));
  
  
  for(m=0; m<mMax;m++)
    {
      for(n=0; n < nMax(m); n++)
	{
	  n_z=ceil((N+2)*pow(Q,2.0/3.0)+0.5-(m+2*n+1)*Q-1);
	  if(n_z>=0)
	    n_zMax(m,n)=n_z;
	  else 
	    n_zMax(m,n)=0;
	}
    }
} 

arma::vec Basis::zPart(arma::vec z, int n)
{
  arma::vec zp=z;
  poly.calcHermite(n+1, z/bz);
  zp=1/sqrt(bz)*z_poly(n)*exp(-(z%z)/(2*bz*bz))%poly.hermite(n);
  return zp;
}

arma::vec Basis::rPart(arma::vec r, int m, int n)
{
  arma::vec rp=r;
  poly.calcLaguerre(abs(m)+1,n+1, r%r/(br*br));
  rp=1/br*r_poly(abs(m),n)*exp(-r%r/(2*br*br))%pow(r/br,abs(m))%poly.laguerre(abs(m),n);
  return rp;
}

arma::mat Basis::basisFunc(int m , int n , int n_z ,arma::vec zVals, arma::vec rVals)
{		
  arma::mat func ;
  func = rPart(rVals, m, n)* (zPart(zVals, n_z).t()); // pourquoi .t() ? -> transpose! pourquoi e(im*theta) = 1 <= imt = 0 mais pourquoi ? 
  return func;
}
