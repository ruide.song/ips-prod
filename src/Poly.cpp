#include "Poly.h"

using namespace std;


arma::vec Poly::Her_poly(int n,arma::vec z)
	{ 
	  arma::vec hn=z;
	  if(n==0) 
	     hn.fill(1);  
	  else if(n==1)
  	     hn=2*z;
	  else
	     hn=(2*z)%Her_poly(n-1,z)-2*(n-1)* Her_poly(n-2,z);
	  return hn; 
}


arma::mat  Poly::calcHermite(int n , arma::vec zVals){
	int l = zVals.n_elem;
	her.zeros(l,n);
	for(int i=0; i<n;i++){
		her.col(i)=Her_poly(i,zVals);	
	}
	return her;
}

arma::vec  Poly::hermite(int i){
	return her.col(i);

}


arma::vec Poly::Lag_poly(int m, int n, arma::vec z)
	{  
	  double i = (double)n;
	  double j = (double)m;
	  arma::vec l=z;
	  if(n==0) 
	     l.fill(1);  
	  else if(n==1)
  	     l=1+j-z;
	  else
	     l=(2+(-z+j-1)/i)%Lag_poly(m,n-1,z)-(1+(j-1)/i)*Lag_poly(m,n-2,z);	

	  return l; 
} 


arma::cube  Poly::calcLaguerre(int m, int n, arma::vec zVals){
	int l = zVals.n_elem;
	lag.zeros(l,n,m);
	for(int i=0; i<m;i++){
	   for(int j=0; j<n;j++){
		lag.slice(i).col(j)=Lag_poly(i,j,zVals);
	}
	}
	return lag;

}
arma::vec  Poly::laguerre(int m, int n){
	return lag.slice(m).col(n);		
}
