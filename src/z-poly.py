from __future__ import division
import math
import numpy

a = numpy.empty(shape=(20,1),dtype = numpy.longdouble)

f = open("z.arma", "w")
for n in range(20):
    a[n,0]= 1.0/math.sqrt(math.pow(2,n)*math.sqrt(math.pi)*math.factorial(n))

numpy.savetxt("z.arma",a)
with open("z.arma", "r+") as f:
    old = f.read()
    f.seek(0)
    f.write("ARMA_MAT_TXT_FN008\n")
    f.write("20 1\n")
    f.write(old)

f.close()

