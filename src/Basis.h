#ifndef BASIS_H
#define BASIS_H

#include "Poly.h"
#include <math.h>

using namespace std;

class Basis
{
	public :
	  double br, bz, Q;
	  int N;

	  int mMax;
	  arma::ivec nMax;
	  arma::imat n_zMax;

	  Poly poly;

	  arma::vec z_poly ;

	  arma::mat r_poly ;
	
	
	Basis(double, double, int, double);
	int fac(int);
	arma::vec zPart(arma::vec , int );
	arma::vec rPart(arma::vec , int , int);
	arma::mat basisFunc(int, int, int ,arma::vec, arma::vec);
};

#endif	
