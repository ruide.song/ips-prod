# Groupe n°12


## Group


| Family name             | First name               | Email                   |
| -------------           |-------------             |-------------            |
| *SONG*                  | *Ruide*                  | *nsly2022335@gmail.com* |
| *Abdou*                 | *Afifi*                  |*abdourahmane.afifi@ensiie.fr*                         |

### Sujet : Local Density of a Nuclear System

**the local density of a nuclear system will be calculated and plotted..(计算核原子密度，并绘制图形)**<br>
**To launch the project:**
* type make all in root folder.
* type make launch to launch the program

### Adresse du backlog

*https://dubrayn.github.io/IPS-PROD/project.html*

